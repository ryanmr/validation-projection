const reduceObjects = (acc, v) => ({ ...acc, ...v })

const PROPERTY_NAMESPACE = Symbol('_validation_property')
const STATE_NAMESPACE = Symbol('_validation_state')
const METHOD_NAMESPACE = '$validation'

const INITIAL_VALIDATOR_STATE = { dirty: false }

export function getComputedValidation(rules, component) {
  let { state, setState } = component
  setState = setState.bind(component)
  const currentRuleState = state[STATE_NAMESPACE]
  const computed = Object.keys(rules)
    .map(ruleKey => {
      const rule = rules[ruleKey]
      const value = rule.value()
      const validatorFns = Object.keys(rule.validators)
        .map(validatorKey => {
          const validatorFn = rule.validators[validatorKey]
          const outcome = validatorFn(value)
          return {
            [validatorKey]: outcome
          }
        })
        .reduce(reduceObjects, {})

      const valid = Object.keys(validatorFns).reduce((acc, key) => acc && validatorFns[key], true)
      const dirty = (currentRuleState[ruleKey] && currentRuleState[ruleKey].dirty) || false
      const error = !valid && dirty

      const dirtySetter = dirtyValue => () => {
        setState({
          [STATE_NAMESPACE]: {
            ...state[STATE_NAMESPACE],
            [ruleKey]: {
              ...state[STATE_NAMESPACE][ruleKey],
              dirty: dirtyValue
            }
          }
        })
      }
      const $touch = dirty ? () => void 0 : dirtySetter(true)
      const $reset = !dirty ? () => void 0 : dirtySetter(false)

      const result = {
        [ruleKey]: {
          validators: validatorFns,
          dirty,
          error,
          valid,
          $touch,
          $reset
        }
      }

      return result
    })
    .reduce(reduceObjects, {})
  return computed
}

export function allValid(validation) {
  const outcome = Object.keys(validation)
    .map(ruleKey => {
      const rule = validation[ruleKey]
      return rule.valid
    })
    .reduce((acc, outcome) => acc && outcome, true)
  return outcome
}

export function extendWithValidation(component, rulesFn) {
  const boundRulesFn = rulesFn.bind(component)
  const rules = boundRulesFn()
  component.state = {
    ...component.state,
    ...getInitialValidationState(rules)
  }
  component[PROPERTY_NAMESPACE] = {
    boundRulesFn,
    previousRules: rules
  }
  component[METHOD_NAMESPACE] = {
    projection() {
      return getDerivedValidation(this)
    },
    update() {
      updateValidationState(component)
    },
    setState(state, callback = () => void 0) {
      validationSetState(component, state, callback)
    }
  }
}

export function validationSetState(component, obj, callback) {
  let { setState } = component
  setState = setState.bind(component)
  const fn = function validationSetStateCallback() {
    updateValidationState(component)
    callback()
  }
  component.setState(obj, fn)
}

export function getDerivedValidation(component) {
  const { boundRulesFn } = component[PROPERTY_NAMESPACE]
  const rules = boundRulesFn()
  return getComputedValidation(rules, component)
}

export function updateValidationState(component) {
  console.log('trying to update state')
  const { boundRulesFn, previousRules } = component[PROPERTY_NAMESPACE]
  const rules = boundRulesFn()
  const newRules = []
  for (const rule in rules) {
    if (rules.hasOwnProperty(rule)) {
      console.log(rule, 'vs', previousRules[rule])
      if (previousRules[rule] === undefined) {
        newRules.push(rule)
      }
    }
  }
  if (newRules.length > 0) {
    const newRuleStateMap = newRules
      .map(ruleKey => {
        return { [ruleKey]: { ...INITIAL_VALIDATOR_STATE } }
      })
      .reduce(reduceObjects, {})
    const setState = component.setState.bind(component)
    const state = component.state
    setState({
      [STATE_NAMESPACE]: {
        ...state[STATE_NAMESPACE],
        ...newRuleStateMap
      }
    })
  }
}

export function getInitialValidationState(rules = {}) {
  const iv = { dirty: false }
  const stateMap = Object.keys(rules)
    .map(ruleKey => {
      return { [ruleKey]: { ...iv } }
    })
    .reduce(reduceObjects, {})
  return {
    [STATE_NAMESPACE]: stateMap
  }
}

export function getRuleAndValidatorKeys(validation) {
  const map = Object.keys(validation)
    .map(ruleKey => {
      const rule = validation[ruleKey]
      const validatorKeys = Object.keys(rule.validators)
      return { [ruleKey]: validatorKeys }
    })
    .reduce(reduceObjects, {})
  return map
}
