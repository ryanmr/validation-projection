export function truthy(value) {
  return !!value
}

export function betweenMinMax(min, max) {
  return value => min < value && value < max
}

export function requiredLength(length) {
  return str => (str ? str.length > length : false)
}

export function isNumber(value) {
  return !isNaN(parseInt(value, 10))
}

export function lessThanTheOther(value1) {
  return value2 => +value1 > +value2
}

export function valueXLessThanY(x, y) {
  return value => value < y - x
}

export function isEvenNumber(value) {
  return +value % 2 === 0
}