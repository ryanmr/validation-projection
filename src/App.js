import React from 'react'
import './App.css'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import ValidationExampleV1 from './v1/ValidationExmaple'
import ValidationExampleV2 from './v2/ValidationExmaple'
import ValidationExampleV3 from './v3/ValidationExmaple'
import ValidationExampleV4 from './v4/ValidationExmaple'

Object.keys(process.env).forEach(env => {
  // prettier-ignore
  console.info(
    "env %c%s:\n%c%s", // eslint-disable-line
    'font-weight: bold',
    env,
    'font-style: italic',
    process.env[env]
  )
})

const Home = () => (
  <div>
    <h2>(react) validation projection project</h2>
    <ul>
      {/* <li><Link to="/v0">v0</Link></li> */}
      <li>
        <Link to="/v1">v1</Link>
      </li>
      <li>
        <Link to="/v2">v2</Link>
      </li>
      <li>
        <Link to="/v3">v3</Link>
      </li>
      <li>
        <Link to="/v4">v4</Link>
      </li>
    </ul>
  </div>
)

const App = () => (
  <div className="App">
    <Route exact path="/" component={Home} />
    {/* <Route path="/v0" component={ValidationExampleV0} /> */}
    <Route path="/v1" component={ValidationExampleV1} />
    <Route path="/v2" component={ValidationExampleV2} />
    <Route path="/v3" component={ValidationExampleV3} />
    <Route path="/v4" component={ValidationExampleV4} />
  </div>
)

const BASEPATH = process.env.REACT_APP_BASEPATH || undefined

const Entry = () => (
  <Router basename={BASEPATH}>
    <App />
  </Router>
)
export default Entry
