import React from 'react'

const reduceObjects = (acc, v) => ({ ...acc, ...v })

const STATE_NAMESPACE = 'validation'
const PREVIOUS_STATE_NAMESPACE = 'previousValidation'

const INITIAL_VALIDATOR_STATE = { dirty: false }

export function getInitialValidationState(rules = {}) {
  const iv = { dirty: false }
  const stateMap = Object.keys(rules)
    .map(ruleKey => {
      return { [ruleKey]: { ...iv } }
    })
    .reduce(reduceObjects, {})
  return {
    [STATE_NAMESPACE]: stateMap,
    [PREVIOUS_STATE_NAMESPACE]: rules
  }
}

export function getRuleAndValidatorKeys(rules) {
  const map = Object.keys(rules)
    .map(ruleKey => {
      const rule = rules[ruleKey]
      const validatorKeys = Object.keys(rule.validators)
      return { [ruleKey]: validatorKeys }
    })
    .reduce(reduceObjects, {})
  return map
}

export class Validation extends React.Component {
  constructor(props) {
    super(props)
    const { rules } = this.props
    this.state = {
      ...getInitialValidationState(rules)
    }
  }

  componentWillReceiveProps(nextProps) {
    const currentRules = getRuleAndValidatorKeys(nextProps.rules)
    const previousRules = getRuleAndValidatorKeys(this.state[PREVIOUS_STATE_NAMESPACE])
    if (currentRules !== previousRules) {
      console.log('the rules are different')
    }
    console.log(currentRules, previousRules)
    // this.setState({ [PREVIOUS_STATE_NAMESPACE]: currentRules })
  }

  getComputedValidation() {
    const { rules } = this.props
    const currentRuleState = this.state[STATE_NAMESPACE]
    const computed = Object.keys(rules)
      .map(ruleKey => {
        const rule = rules[ruleKey]
        const value = rule.value()
        const validatorFns = Object.keys(rule.validators)
          .map(validatorKey => {
            const validatorFn = rule.validators[validatorKey]
            const outcome = validatorFn(value)
            return {
              [validatorKey]: outcome
            }
          })
          .reduce(reduceObjects, {})

        const valid = Object.keys(validatorFns).reduce((acc, key) => acc && validatorFns[key], true)
        const dirty = (currentRuleState[ruleKey] && currentRuleState[ruleKey].dirty) || false
        const error = !valid && dirty

        const dirtySetter = dirtyValue => () => {
          this.setState({
            [STATE_NAMESPACE]: {
              ...this.state[STATE_NAMESPACE],
              [ruleKey]: {
                ...this.state[STATE_NAMESPACE][ruleKey],
                dirty: dirtyValue
              }
            }
          })
        }
        const $touch = dirty ? () => void 0 : dirtySetter(true)
        const $reset = !dirty ? () => void 0 : dirtySetter(false)

        const result = {
          [ruleKey]: {
            validators: validatorFns,
            dirty,
            error,
            valid,
            $touch,
            $reset
          }
        }

        return result
      })
      .reduce(reduceObjects, {})
    return computed
  }

  render() {
    const { children } = this.props
    const computedValidation = this.getComputedValidation()
    return children(computedValidation)
  }
}
