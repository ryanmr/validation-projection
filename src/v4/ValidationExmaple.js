import React from 'react'
import {
  getRuleAndValidatorKeys,
  Validation
} from './ValidationProjector'
import {
  truthy,
  isEvenNumber,
  isNumber,
  requiredLength,
  valueXLessThanY
} from './validation-functions'

const inline = (...args) => args.reduce((acc, o) => ({ ...acc, ...o }), {})

const styles = {
  input: {
    padding: '0.5rem',
    fontSize: '1.1rem'
  },
  help: {
    fontSize: '0.8rem',
    display: 'block',
    marginTop: '3px'
  },
  valid: {
    border: '2px solid #0f0'
  },
  invalid: {
    border: '2px solid #f00'
  }
}

const errorStyles = validator => {
  if (validator.error) {
    return styles.invalid
  }
  if (validator.dirty && validator.valid) {
    return styles.valid
  }
  return {}
}

class ValidationExample extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      number1: '',
      number2: '',
      showNumber2: false,
      submitted: false
    }
  }

  rules() {
    let rules = {
      name: {
        value: () => this.state.name,
        validators: {
          truthy,
          length: requiredLength(3)
        }
      },
      number1: {
        value: () => this.state.number1,
        validators: {
          isNumber,
          isEvenNumber
        }
      }
    }
    if (this.state.showNumber2) {
      rules = {
        ...rules,
        number2: {
          value: () => this.state.number2,
          validators: {
            isNumber,
            valueXLessThanY: valueXLessThanY(10, this.state.number1)
          }
        }
      }
    }
    return rules
  }

  render() {
    const rules = this.rules()
    return (
      <Validation rules={rules}>
        {validation => (
          <div>
            <div>
              <strong>validation rules</strong>
              <code>
                <pre>{JSON.stringify(getRuleAndValidatorKeys(rules), null, 2)}</pre>
              </code>
              <hr />
              <strong>state</strong>
              <code>
                <pre>{JSON.stringify(this.state, null, 2)}</pre>
              </code>
              <hr />
              <strong>validation projection</strong>
              <code>
                <pre>{JSON.stringify(validation, null, 2)}</pre>
              </code>
              <hr />
              <div>
                <p>
                  <input
                    type="text"
                    style={inline(styles.input, errorStyles(validation.name))}
                    placeholder="name"
                    value={this.state.name}
                    onChange={e => {
                      validation.name.$touch()
                      this.setState({ name: e.target.value })
                    }}
                  />
                  <span style={inline(styles.help)}>should be at least 4 characters</span>
                </p>
                <p>
                  <input
                    type="text"
                    style={inline(styles.input, errorStyles(validation.number1))}
                    placeholder="number 1"
                    value={this.state.number1}
                    onChange={e => {
                      validation.number1.$touch()
                      this.setState({ number1: e.target.value })
                    }}
                  />
                  <span style={inline(styles.help)}>should be an even number</span>
                </p>
                <p>
                  <label>
                    Show Number 2:
                    <input
                      type="checkbox"
                      onChange={e => {
                        this.setState({ showNumber2: !this.state.showNumber2 })
                      }}
                      value={this.state.showNumber2}
                    />
                  </label>
                </p>
                {this.state.showNumber2 && (
                  <p>
                    <input
                      type="text"
                      style={inline(styles.input, errorStyles(validation.number2))}
                      placeholder="number 2"
                      value={this.state.number2}
                      onChange={e => {
                        validation.number2.$touch()
                        this.setState({ number2: e.target.value })
                      }}
                    />
                    <span style={inline(styles.help)}>
                      should be a number, that is <i>10 less</i> than number 1
                    </span>
                    {validation.number2.error && (
                      <span
                        style={inline(styles.help, {
                          color: 'red'
                        })}
                      >
                        huge error! {this.state.number2} &lt; {this.state.number1} - 10
                      </span>
                    )}
                  </p>
                )}
              </div>
            </div>
          </div>
        )}
      </Validation>
    )
  }
}

export default ValidationExample
