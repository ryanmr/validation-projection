const reduceObjects = (acc, v) => ({ ...acc, ...v })

const NAMESPACE = '_validation'

export function getComputedValidation(validation, component) {
  let { state, setState } = component
  setState = setState.bind(component)
  const currentRuleState = state[NAMESPACE]
  const computed = Object.keys(validation)
    .map(ruleKey => {
      const rule = validation[ruleKey]
      const value = rule.value()
      const validators = Object.keys(rule.validators)
        .map(validatorKey => {
          const validator = rule.validators[validatorKey]
          const outcome = validator(value)
          return {
            [validatorKey]: outcome
          }
        })
        .reduce(reduceObjects, {})

      const dirty = currentRuleState[ruleKey].dirty
      const valid = Object.keys(validators).reduce(
        (acc, key) => acc && validators[key],
        true
      )
      const error = !valid && dirty
      const $touch = dirty
        ? () => void 0
        : () => {
            setState({
              [NAMESPACE]: {
                ...state[NAMESPACE],
                [ruleKey]: {
                  ...state[NAMESPACE][ruleKey],
                  dirty: true
                }
              }
            })
          }
      const $reset = !dirty
        ? () => void 0
        : () => {
            setState({
              [NAMESPACE]: {
                ...state[NAMESPACE],
                [ruleKey]: {
                  ...state[NAMESPACE][ruleKey],
                  dirty: false
                }
              }
            })
          }

      const result = {
        [ruleKey]: {
          validators: validators,
          dirty,
          error,
          valid,
          $touch,
          $reset
        }
      }

      return result
    })
    .reduce(reduceObjects, {})
  return computed
}

export function allValid(validation) {
  const outcome = Object.keys(validation)
    .map(ruleKey => {
      const rule = validation[ruleKey]
      return rule.valid
    })
    .reduce((acc, outcome) => acc && outcome, true)
  return outcome
}

export function getInitialValidationState(validation) {
  const iv = { dirty: false }
  const stateMap = Object.keys(validation)
    .map(ruleKey => {
      const rule = validation[ruleKey]
      return { [ruleKey]: { ...iv } }
    })
    .reduce(reduceObjects, {})
  return {
    [NAMESPACE]: stateMap
  }
}

export function getRuleAndValidatorKeys(validation) {
  const map = Object.keys(validation)
    .map(ruleKey => {
      const rule = validation[ruleKey]
      const validatorKeys = Object.keys(rule.validators)
      return { [ruleKey]: validatorKeys }
    })
    .reduce(reduceObjects, {})
  return map
}
