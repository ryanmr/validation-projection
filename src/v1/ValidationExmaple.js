import React from 'react'
import {
  getRuleAndValidatorKeys,
  getComputedValidation,
  getInitialValidationState,
  allValid
} from './ValidationProjector'

function truthy(value) {
  return !!value
}

function betweenMinMax(min, max) {
  return value => min < value && value < max
}

function requiredLength(length) {
  return str => (str ? str.length > length : false)
}

function isNumber(value) {
  return !isNaN(parseInt(value))
}

function lessThanTheOther(value1) {
  return value2 => +value1 > +value2
}

function valueXLessThanY(x, y) {
  return value => value < y - x
}

function isEvenNumber(value) {
  return +value % 2 === 0
}

const inline = (...args) =>
  args.reduce((acc, o) => ({ ...acc, ...o }), {})

const styles = {
  input: {
    padding: '0.5rem',
    fontSize: '1.1rem'
  },
  help: {
    fontSize: '0.8rem',
    display: 'block',
    marginTop: '3px'
  },
  valid: {
    border: '2px solid #0f0'
  },
  invalid: {
    border: '2px solid #f00'
  }
}

const errorStyles = validator => {
  if (validator.error) {
    return styles.invalid
  }
  if (validator.dirty && validator.valid) {
    return styles.valid
  }
  return {}
}

class ValidationExample extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      number1: '',
      number2: '',
      submitted: false
    }
    const rules = this.rules()
    this.state = {
      ...this.state,
      ...getInitialValidationState(rules)
    }
    console.log('initial state', this.state)
  }

  rules() {
    const rules = {
      name: {
        value: () => this.state.name,
        validators: {
          truthy,
          length: requiredLength(3)
        }
      },
      number1: {
        value: () => this.state.number1,
        validators: {
          isNumber,
          isEvenNumber
        }
      },
      number2: {
        value: () => this.state.number2,
        validators: {
          isNumber,
          valueXLessThanY: valueXLessThanY(
            10,
            this.state.number1
          )
        }
      }
    }
    return rules
  }

  projection() {
    return getComputedValidation(this.rules(), this)
  }

  render() {
    const rules = this.rules()
    const validation = this.projection()
    console.log('validation', validation)
    return (
      <div>
        <div>
          <strong>validation rules</strong>
          <code>
            <pre>
              {JSON.stringify(
                getRuleAndValidatorKeys(rules),
                null,
                2
              )}
            </pre>
          </code>
          <hr />
          <strong>state</strong>
          <code>
            <pre>{JSON.stringify(this.state, null, 2)}</pre>
          </code>
          <hr />
          <strong>validation projection</strong>
          <code>
            <pre>{JSON.stringify(validation, null, 2)}</pre>
          </code>
          <hr />
          <div>
            <p>
              <input
                type="text"
                style={inline(
                  styles.input,
                  errorStyles(validation.name)
                )}
                placeholder="name"
                value={this.state.name}
                onInput={e => {
                  validation.name.$touch()
                  this.setState({ name: e.target.value })
                }}
              />
              <span style={inline(styles.help)}>
                should be at least 4 characters
              </span>
            </p>
            <p>
              <input
                type="text"
                style={inline(
                  styles.input,
                  errorStyles(validation.number1)
                )}
                placeholder="number 1"
                value={this.state.number1}
                onInput={e => {
                  validation.number1.$touch()
                  this.setState({ number1: e.target.value })
                }}
              />
              <span style={inline(styles.help)}>
                should be an even number
              </span>
            </p>
            <p>
              <input
                type="text"
                style={inline(
                  styles.input,
                  errorStyles(validation.number2)
                )}
                placeholder="number 2"
                value={this.state.number2}
                onInput={e => {
                  validation.number2.$touch()
                  this.setState({ number2: e.target.value })
                }}
              />
              <span style={inline(styles.help)}>
                should be a number, that is <i>10 less</i> than
                number 1
              </span>
              {validation.number2.error && (
                <span
                  style={inline(styles.help, {
                    color: 'red'
                  })}
                >
                  huge error! {this.state.number2} &lt;{' '}
                  {this.state.number1} - 10
                </span>
              )}
            </p>
            <p>
              <button
                onClick={() => {
                  this.submitForm()
                }}
              >
                done
              </button>
            </p>
            {this.state.submitted && (
              <p>The form is valid and you are good to go!</p>
            )}
          </div>
        </div>
      </div>
    )
  }

  submitForm() {
    const validation = this.projection()
    const valid = allValid(validation)
    if (valid) {
      this.setState({ submitted: true })
    }
  }
}

export default ValidationExample
